;; FIXME Shorten the excessively long docstrings, generated
;; by the `build-farm-define-entry-type build' macro.
((emacs-lisp-mode
  (byte-compile-warnings . (not docstrings))))
